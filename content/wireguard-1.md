+++
title = "Le Réseau de zéro (partie 2)"
date = 2022-01-27
draft = false

[taxonomies]
categories = ["Réseau"]
tags = ["linux", "sys admin", "réseau"]

[extra]
lang = "fr"
toc = true
show_comment = true
math = false
mermaid = false
cc_license = true
outdate_warn = true
outdate_warn_days = 120
metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="Le réseau de zéro (partie 2)" },
    { name = "twitter:image", content="https://i.ibb.co/s16nvmc/img.png" },
]
+++

Bonjour à toutes et tous :)

Chose promise chose due, voici la deuxième partie du prologue de l'article sur Wireguard.

> Je vous jure un jour cet article verra le jour 🤣

Mais pour ça j'ai envie de creuser un petit peu les choses.

Et donc aujourd'hui, nous allons détailler la communication entre deux
noeuds <span style="color:#ff3333">rouges</span> d'un réseau.

![sous réseau](../assets/images/wireguard/sub-network.png)

Nous verrons plus tard comment un noeud <span style="color:#ff3333">rouge</span> peut discuter avec un noeud <span style="color:#33c1ff">bleu</span>, c'est un poil plus complexe.

C'est parti ! 😁

## L'adresse MAC

L'adresse MAC ou **M**edia **A**ccess **C**ontrol est un identifiant unique (en théorie 😛 ) qui est affecté à chaque interface du réseau. 

Pour les révisions c'est [ici](/wireguard-0/#interface).

Cet idenfiant se compose de 48 bits, généralement écrits sous forme hexadécimale.

En fonction des systèmes d'exploitation, la représentation peut-être différente.

![représentation des adresses MAC selon l'OS](../assets/images/reseau-2/adresse-mac.png).

Cette adresse est en quelque sorte la plaque d'immatriculation de votre interface réseau.

Nous allons donc pouvoir améliorer notre schéma en affectant non plus seulement une adresse IP à notre interface mais aussi une adresse MAC.

Pour m'épagner l'écriture complète des adresses MAC dans la suite des exemples. Je vais utiliser une notation simplifiée.

![notation simplifiée d'adresse MAC](../assets/images/reseau-2/adresse-mac-simple.png)

Ce qui nous permet d'écrire ceci:

![interface muni d'une adresse MAC](../assets/images/reseau-2/interface-avec-mac.png)

Ceci est une machine possédant une interface **eth0** qui a comme adresse IP `192.168.1.12` et comme adresse MAC `.e6f1`.

### Adresse MAC de broadcast

Tout comme il existe une adresse IP de [broadcast](/wireguard-0/#adresse-de-broadcast).

![adresse MAC de broadcast](../assets/images/reseau-2/broadcast-mac.png)

Cette adresse va être extrêmement importante par la suite. 😉

## Le fonctionnement des switchs

### Les ports d'un switch

Je vais révéler un terrible secret. 🧐  

Un switch est incapable de comprendre une adresse IP. Les seules choses qu'ils connaissent sont les **adresses MAC**! 😮

Dans les faits, les switchs possèdent une table qui associe un port à une adresse MAC et donc extension l'ordinateur qui y est branché.

Par exemple, un ordinateur qui possède une adresse MAC `.ae20` sur le port 1. Donnerait cette association.

```
port 1 -> .ae20
```

Et on peut avoir la même chose pour tous les ports qui y sont branchés.

```
port 2 -> .ef12
port 3 -> .6af1
```

Si on représente graphiquement ces associations:

![Un LAN classique](../assets/images/reseau-2/switch.png)

### Transmettre l'information

Lorsque l'ordinateur possédant l'adresse MAC `.ae20` veut communiquer avec l'ordinateur qui a l'adresse MAC `.ef12`, il va se passer ceci.

#### Trame réseau

Pour permettre à notre message d'être compris par le switch nous avons besoin de lui rajouter des informations supplémentaires.

Le message composé de nos données et du bloc d'information indiquant entre autre, de l'adresse MAC de destination et de source est appelé une `trame` ou `frame` en anglais.

Ce bloc d'information supplémentaire par rapport aux données que l'on veut transmettre, se nomme un `entête` ou `header`. 

![définition d'une trame ethernet](../assets/images/reseau-2/frame.png)

#### Le voyage d'une trame

A l'état initial nous sommes ainsi.

Le switch connait tout le monde et l'ordinateur de gauche souhaite communiquer avec l'ordinateur de droite.

![l'ordinateur de gauche initie un envoie de messages](../assets/images/reseau-2/switch2.png)

L'ordinateur de gauche génère une trame réseau, qui contient deux parties:
- Les données à transmettre ( ici en blanc )
- Les addresses MAC à usage du switch: ( ici en orange )
  - de destination : **DST**
  - de source      : **SRC**

![un message avec l'information MAC](../assets/images/reseau-2/mac_nomen.png).

Ce message, sort alors de l'interface et suit le fil qui le conduit jusqu'au switch.

![l'ordinateur de gauche initie un envoie de messages](../assets/images/reseau-2/switch3.png)

Le message arrive dans le switch par le **port 1**.

![le message arrive dans le switch](../assets/images/reseau-2/switch4.png)

L'adresse MAC de destination du message est lue par le switch. Elle est détectée comme étant `.ef12`. 

Le switch, grâce à sa table de correspondance, "sait" que le **port 2** y est associé.

Il va alors créer un pont entre le port d'entrée `1` et le port de sortie `2`.

![le switch réalise la jonction de ports](../assets/images/reseau-2/switch5.png)

Le message suit alors le "câble" ainsi créé pour atteindre la destination voulue.

![le message arrive à destination](../assets/images/reseau-2/switch6.png)

#### Généralisation

Bien sûr, ce comportement fonctionne avec plus de 2 ports branchés.

![le message arrive à destination](../assets/images/reseau-2/switch7.png)

Le PC possédant l'adresse mac `.ae12` et branché sur le **port 1** du switch envoie un message en direction de l'adresse mac `.32a9`.

Le switch connait le port sur lequel est branché la machine qui correspond à l'adresse mac `.32a9`.

Ici l'adresse mac `.32a9` est associée au **port 3**.

Il réalise la jonction entre les ports `1` et `3`.

Le message peut alors arriver à destination.

### Broadcast réseau

Un peu plus haut dans l'article, je vous ai parlé de [l'adresse mac de broadcast](/wireguard-1/#adresse-mac-de-broadcast) nous allons tout de suite voir sa spécificité.

Lorsque l'adresse MAC de destination est `.ffff` alors le switch a pour comportement de dupliquer le message sur l'ensemble des ports excepté celui d'entrée.

Ce processus se nomme **innondation**.

![innondation réseau par le switch](../assets/images/reseau-2/switch8.png)

Il nous sera très pratique dans la suite de nos besoins.

## Et les IPs dans tout ça ?

Mais allez me dire:

> Moi quand je parle à une machine c'est par adresse IP et pas par adresse MAC

Et vous avez tout à fait raison ! 😁

En fait, le bloc données n'est pas ce qu'il semble être au premier abord. 

Il n'est pas uniquement constitué des données veut transmettre. Nous avons, en autre, aussi "caché" l'adresse IP de destination.

![encapsulation des données IP](../assets/images/reseau-2/ip.png)

Dont on peut simplifier l'écriture ainsi:

![encapsulation des données IP](../assets/images/reseau-2/ip2.png)

Ce qui nous permet de rajouter une couche d'information à notre message qui cette fois ci ne concerne plus l'adresse MAC mais l'adresse IP.

L'ensemble _header_ IP et données est appelé un `paquet`.

Lorsque paquet transite sur le réseau, il est toujours inclu dans une trame et donc possède un _header_ MAC.

## Et mon port j'en fait quoi ? 🐖

Dans l'article précédent je vous avait parlé de la notion de [port logique](/wireguard-0/#ports-logiques).

Qui consistait à contacter le service d'une machine au moyen d'un numéro.

On va voir comment ce numéro est défini dans les données transmises par un paquet.

Tout comme, lorque vous rédigez une lettre vous allez écrire l'adresse du destinataire sur le recto de l'enveloppe. 

Et si vous désirez que ce destinataire puisse vous répondre vous avez la possibilité d'inscrire votre adresse sur le rabat de l'enveloppe au verso.

![Verso d'une enveloppe](../assets/images/reseau-2/enveloppe_verso.png)

### Port dynamique

Imaginons vous naviguez sur internet, vous souhaitez afficher twitter.com comme on l'a réalisé dans l'exemple de [l'article précédent](/wireguard-0/#le-serveur-dns).

Vous allez recevoir une IP sur laquelle vous rendre et étant donné qu'il s'agit d'HTTPS, votre navigateur va s'y connecter avec le port normalisé 443.

Or, pour que twitter.com soit dans la capacité de vous répondre, il faut que votre ordinateur définisse un port de réponse.

Pour cela il va piocher un numéro aléatoirement choisi dans la liste des ports qui ne sont pas encore utilisé par une application de votre machine.

Sur un windows par exemple, cette plage port de réponse s'étend de **49152** à **65535**.

On va par exemple choisir le `60816` pour notre exemple.

### Segment

Pour stocker cette information de port, nous allons définir un nouvel header dans le paquet envoyé à twitter qui contiendra:
- l'IP de destination : **104.244.42.129**
- notre propre IP : **82.109.194.218**
- le port lequel on souhaite communiquer : **443**
- le port de réponse sur lequel on invite twitter à nous répondre.: **60816**
- on désire communiquer en **TCP**

Voici le paquet ainsi créé:

![paquet TCP](../assets/images/reseau-2/paquet.png)

Que l'on va lui aussi détailler.

![paquet TCP détaillé](../assets/images/reseau-2/paquet2.png)

Les données et leur information de ports sont appelés un `segment`.

Et d'ailleurs lorsque vous demandé l'adresse IP de twitter au serveur DNS vous avez aussi réalisé une requête, mais celle-ci UDP sur le port **53** de l'IP **1.1.1.1**.

Ce qui nous donne ce paquet par exemple.

![paquet UDP](../assets/images/reseau-2/paquet3.png)

Ce paquet explique au serveur DNS sur quel port nous répondre lorsqu'il nous renverra l'IP de twitter.com, ici le **51203**.

## ARP

Dans la [partie précédente](/wireguard-1/#la-verite-sur-les-switchs), je vous ai expliqué que les switchs étaient incapables de comprendre ce qu'était une IP et donc de traiter un paquet.

Or, dans notre exemple nous avons défini des IP de destination et de source.

> Quel mécanisme permet la construction de la trame nécessaire à l'acheminement au travers du réseau et plus particulièrement au travers des switchs ?

Ce mécanisme s'appelle l'ARP (**A**ddress **R**esolution **P**rotocol) ou PRA (**P**rotocole de **R**ésolution d'**A**dresses). Mais personne n'emploie ce terme. ^^'

### Table ARP

Chaque ordinateur possède une table d'association entre les adresses MAC qu'il connait et leur adresse IP correspondante.

Cette table est appelée la **table ARP**.

### Problème à résoudre

Par exemple, si nous avons le réseau suivant:

![situation initiale](../assets/images/reseau-2/arp.png)

Sur le dessin, j'ai représenté les **tables ARP** sont ici vides.

Dans notre exemple nous avons l'ordinateur 1 qui souhaite communiquer avec l'ordinateur 2.

Il connait l'IP de l'ordinateur 2, c'est l'adresse `192.168.10.13`.

Notre ordinateur est donc capable de concevoir un paquet possédant les information :
- d'IP de destination
- d'IP source

![paquet IP](../assets/images/reseau-2/arp2.png)

Le souci est d'être capable de concevoir la trame qui permettra d'acheminer le message dans le réseau.

![trame incomplète](../assets/images/reseau-2/arp3.png)

Tout l'objet du protocole ARP va être de trouver un moyen de remplir les points d'interrogations.

### Fonctionnement

Pour obtenir l'adresse MAC associée à l'IP **192.168.10.13**. L'ordinateur 1 va créer un paquet ARP. 

Celui-ci possède un certain nombre de données dont:
- HW_SRC: l'adresse MAC du demandeur: **.ae12**
- IP_SRC: l'adresse IP du demandeur: **192.168.10.2**
- IP_TRG: l'adresse IP demandée: **192.168.10.13**

![paquet ARP](../assets/images/reseau-2/arp4.png)

Il ne possède pas de champ de données.

Pour qu'il puisse transiter dans le réseau, il lui faut une trame car souvenez vous les paquets ne sont pas compris par les switchs.

Pour constituer notre trame, nous allons nous appuyer sur l'addresse MAC de [broadcast](/wireguard-1/#broadcast).

Ce qui nous donne la trame suivante:
- Type de trame : **ARP**
- Adresse MAC de source : **.ae12**
- Adresse MAC de destination : **.ffff**

![trame ARP](../assets/images/reseau-2/arp5.png)

Cette trame ARP sort de l'interface de l'ordinateur 1 et se propage dans le réseau en suivant le câble en direction du switch.

![trame ARP arrive au switch](../assets/images/reseau-2/arp6.png)

Lorsque la trame arrive dans le switch par le port 1, il se passe une série d'actions.

Tout d'abord la trame est lue et on défini la destination qu'elle doit prendre. 

Or, ici la destination est l'adresse de broadcast, on va donc procéder à une innondation réseau.

Il se passe également une seconde chose. La trame étant de type ARP, on vient lire le champ contenant l'adresse MAC de source, ici `.ae12`.

Et connaissant déjà de quel port provient la trame analysée, nous pouvons remplir la table association du switch.

![remplissage de table d'association du switch par analyse de la trame ARP](../assets/images/reseau-2/arp7.png)

La trame ARP est alors dupliquée sur l'ensemble des ports excepté le port 1.

![processus d'innondation ARP](../assets/images/reseau-2/arp8.png)

Maintenant nous devons différencier 2 cas:
- le paquet ARP est destiné à l'hôte ( cas de l'hôte 2 )
- le paquet ARP n'est pas destiné à l'hôte ( cas de l'hôte 3 )

### Paquet ARP ignoré

Voyons le cas de l'hôte 3. La trame ARP possédant l'adresse de MAC de broadcast est acceptée par l'interface de l'hôte 3.

![trame ARP acceptée](../assets/images/reseau-2/arp9-1.png)

Par contre l'adresse IP de cible du paquet ARP est différent de celui de l'hôte. Ne sachant pas quoi en faire il drop le paquet ARP sans se poser de question.

![paquet ARP ignoré](../assets/images/reseau-2/arp9-2.png)

### Paquet ARP accepté 

Cette fois-ci, nous passons au traitement de notre paquet ARP par l'hôte 2 à qui le paquet ARP est réellement destiné.

![paquet ARP traité et table ARP complétée](../assets/images/reseau-2/arp10-1.png)

Le paquet ARP ayant l'IP de destination, il est traité au lieu d'être ignoré contrairement au cas de l'hôte 3.

Le paquet ARP est alors lu et les informations de l'adresse de MAC de source ainsi que l'adresse IP de source sont utilisés pour compléter la table ARP précédemment vide.

### Création de la réponse ARP

Pour répondre à la requête ARP l'hôte va devoir créer un nouveau paquet ARP qui va contenir les informations suivantes:

- HW_SRC : sa propre adresse MAC : **.f32a**
- HW_TRG : l'adresse MAC de l'hôte qui a envoyé la requête : **.ae12**
- IP_SRC : sa propre adresse IP : **192.168.10.13**
- IP_TRG : l'adresse IP de l'hôte qui a envoyé la requête : **192.168.10.2**

Remarquez que l'adresse de source a changé entre la requête et sa réponse. C'est normal les rôles sont inversés.

![création du paquet ARP de réponse](../assets/images/reseau-2/arp11-1.png)

Il faut alors une trame recopiant ces information d'adresse MAC:

![création du paquet ARP de réponse](../assets/images/reseau-2/arp11-2.png)

Cette trame se dirige alors vers le switch, via port 2.

![la trame sort de l'interface et rentre dans le switch](../assets/images/reseau-2/arp12.png)

La trame est alors analysée par le switch qui peut remplir sa table d'association avant de rediriger la trame vers le port 1 étant donné que l'adresse de destination est la `.ae12`.

![la trame sort du switch](../assets/images/reseau-2/arp13.png)

Cette trame arrive dans l'hôte par l'interface qui possède l'adresse MAC de destination. La trame est accepte la trame est acceptée.

![le paquet ARP de réponse est traité et la table ARP complétée](../assets/images/reseau-2/arp14-1.png)

Le paquet ARP possède également l'IP destination correcte, ce qui permet de compléter la table ARP.

![le paquet ARP de réponse est traité et la table ARP complétée](../assets/images/reseau-2/arp14-2.png)

A l'issu de cet échange de paquets ARP entre les hôtes 1 et 2, nous retrouvons avec la situation suivante:

![situation après échange de paquet ARP](../assets/images/reseau-2/arp15.png)

Les hôtes 1 et 2 connaissent les adresses MAC de l'un de l'autre. 

De plus le switch, connait désormais les adresses MAC associées aux ports 1 et 2.
## Envoyé un paquet IP sur le réseau

Imaginez que vous ouvriez un onglet dans votre navigateur et que vous tapiez l'URL `https://192.168.10.13`.

> Que va-t-il se passer ?

A chaque fois que vous ouvrez un onglet, un nouveau processus est créé et un port lui est attribué. Ici par exemple, son port dynamique sera le **60816**.

Lorsque le navigateur désire récupérer le contenu d'une page web sur un serveur, il va venir définir un message qui aura cette forme:

```
GET / HTTP1.1
```

Ceci permet de récupérer la page à la racine du serveur.

Nous avons ainsi notre brique `DATA`.

Nous pouvons aussi créer le segment TCP correspondant car nous savons:

- Que HTTPS utilise le **TCP**
- Que le port de destination du protocole HTTPS est **443**
- Que le port le port dynamique affecté à l'onglet est **60816** 

![l'onglet créé une requête](../assets/images/reseau-2/travel7.png)

Nous connaissons:
- Notre IP : **192.168.10.2**
- L'IP de destination : **192.168.10.13**

Nous pouvons créer le paquet correspondant.

![le segment est encapsulé dans un paquet](../assets/images/reseau-2/travel8.png)

Rappelez-vous que notre paquet va devoir traverser un switch qui ne comprend pas la notion d'adresse IP.

Nous devons absolument définir une adresse MAC de destination.

![trame incomplète](../assets/images/reseau-2/arp3.png)

Fort heureusement, la vie étant bien faite nous avons détaillé le fonctionnement du protocole ARP.

Qui comme par hasard nous permet de récupérer l'adresse MAC associée à une adresse IP. C'est fou les coïcidences, hein ?! 🤣

Grâce au protocole ARP, nous avons capacité de récupérer cette adresse MAC de destination, qui est désormais est stockée dans la table ARP de l'hôte.

Nous savons désormais que l'hôte qui possède l'IP `192.168.10.13` a pour adresse MAC `.f32a`.

Nous pouvons ainsi compléter la trame avec la destination.

![trame complétée](../assets/images/reseau-2/travel9.png)

Ouf! On a enfin notre trame prête à être propager dans le réseau !! 🥳

Notre trame arrive sur le port 1 du switch, son adresse MAC de destination est alors lue.

![commutation au travers du switch](../assets/images/reseau-2/travel4.png)

Le switch va alors commuter la trame vers le port 2.

![trame commutée](../assets/images/reseau-2/travel5.png)

La trame arrive alors à l'intérieur de la machine 2 où elle y est analysée. La destination de la trame correspondant à l'adresse MAC de la machine. La trame est acceptée et le paquet transmis au reste du système.

![trame analysée et acceptée](../assets/images/reseau-2/travel6.png)

Le paquet est alors transmis pour être à son tour analysé. 

Remarquez, on ne perd pas les informations précédemment analysées, ça nous sera pratique dans un prochain article. ^^'

![paquet analysé et accepté](../assets/images/reseau-2/travel10.png)

C'est au tour du segment de ce faire analyser pour lui affecter une destination de processus.

Ici le port de destination est le **443* autrement dit le serveur HTTP. 

(dans les faits c'est un petit peu plus complexe que ça mais faut bien garder de la matière pour un autre article non ? 😁).

![segment analysé](../assets/images/reseau-2/travel11.png)

Nous avons finalement notre requête HTTP qui atteint le serveur Web.

```
GET / HTTP1.1
```

Celui-ci, maintenant, peut répondre:

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Ma super page</title>
  </head>
  <body>
    <h1>Bonjour !!</h1>
  </body>
</html>
```

Et donc on fait le circuit dans le sens inverse:

On défini à la destination du header de transport: le précédent port de source de la requête qui nous a été affecté pour que l'on puisse répondre. 

![création du segment de réponse](../assets/images/reseau-2/travel12.png)

Même combat pour l'adresse IP

![création du paquet de réponse](../assets/images/reseau-2/travel13.png)

Et finalement, la trame utilise aussi la table ARP pour venir définir sa destination.

![création de la trame de réponse](../assets/images/reseau-2/travel14.png)

La trame sort sur le réseau.

![trame de réponse sur le réseau](../assets/images/reseau-2/travel15.png)

Le switch commute la trame à destination

![trame de réponse commutée sur le réseau](../assets/images/reseau-2/travel16.png)

On passe les étapes d'analyse et d'acceptation et on saute tout de suite à la redirection du message vers l'onglet qui a demandé la page:

![trame de réponse sur le réseau](../assets/images/reseau-2/travel17.png)

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Ma super page</title>
  </head>
  <body>
    <h1>Bonjour !!</h1>
  </body>
</html>
```

Et voilà on a notre page !!! 🥳

## Conclusion

Et voilà pour la première partie de l'explication de certains protocoles réseaux.

On a vu comment deux machines d'un même réseau étaient capable de communiquer.

Nous verrons dans l'article prochain, comment deux machines de deux réseaux différents peuvent le faire aussi.

Je vous remercie pour votre lecture et je vous donne rendez-vous pour la suite (date non fixée) ❤️