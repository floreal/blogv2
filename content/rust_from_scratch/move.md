+++
title = "Move, Copy et Clone"
date = 2022-10-31
draft = false
path = "/rust/move"

[taxonomies]
categories = ["Rust par le métal"]
tags = ["rust", "rust_par_le_metal"]

[extra]
permalink = "/rust/move"
lang = "fr"
toc = true
show_comment = true

metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="Rust par le Métal: Move, Copy et Clone" },
    { name = "twitter:image", content="https://lafor.ge/assets/thumbails/rust_move2.png" },
]
+++

## Move

Lorsqu'une [fonction](/rust/fonction) désire obtenir l'[ownership](/rust/ownership) d'une [variable](/rust/variable).

On vient déplacer la variable du contexte appelant vers le contexte appelé.

Prenons une [structure](/rust/structure) **Toto**.

{{ image(path="rust/move/struct.png", width="55%") }}

On déclare une [fonction](/rust/fonction) `function_1`, celle-ci ne fait rien à part prendre l'[ownership](/rust/ownership) de la [variable](/rust/variable) passé en paramètre.

{{ image(path="rust/move/f1.png", width="100%") }}

On déclare dans un [main](/rust/main), une [variable](/rust/variable) `toto` de type **Toto**.

{{ image(path="rust/move/main.png", width="55%") }}

Lorsque l'on passe en paramètre la [variable](/rust/variable) `toto` à la [fonction](/rust/fonction) `function_1`, on dit que l'on déplace ou *move* la variable vers `function_1`.

{{ image(path="rust/move/move.png", width="70%") }}

C'est comme si vous aviez un carton dans une maison 1 et que vous le déplaciez dans une maison 2.

{{ image(path="rust/move/move2.png", width="100%") }}

Et donc très logiquement si on réitère l'expérience, ça coince.

{{ image(path="rust/move/move3.png", width="100%") }}

Pourquoi, ça coince ?

Et bien, votre carton n'a pas le don d'ubiquité (ce mot est cadeau pour vous 😁). Autrement dit, elle ne peut pas à la fois être dans la maison `main` et dans la maison `function_1`.

Donc si les déménageurs arrivent, ils ne trouveront pas le carton dans la maison `main`! Et ne pourront pas le déplacer vers la maison `function_1*`

{{ image(path="rust/move/move4.png", width="100%") }}

## Clone

Pour remédier à cette situation, nous allons devoir implémenter un [trait](/rust/trait).

Celui-ci ce nomme `Clone`.

Il ne dispose que d'une fonction à implémenter.

```rust
trait Clone {
    fn clone(&self) -> Self
}
```

Ce trait renvoit une duplication de lui-même, cela impliquant que chaque champ de la structure qui l'implémente doivent eux-aussi être clonable.

Pour notre structure *Toto* ce n'est pas un soucis.

```rust
impl Clone for Toto {
    fn clone(&self) {
        *self
    }
}
```

On [déréférence](/rust/reference) le **self** et on renvoit une copie.

Un clone est déclenché de manière consciente et explicite par le développeur.

```rust
fn main() {
    let toto = Toto;
    let toto_clone = toto.clone(); // l'opération de clone se fait ici
}
```
{% warning() %}
Si ce que vous clonez est gros, cela peut avoir des conséquences sur les performances de votre programme.
```rust
struct BigData {
    data: [u8; 10000000000]
}
```

Chaque `.clone()` aura pour effet de dupliquer chaque valeur du tableau.

Si les opérations de clonage sont trop fréquentes, cela risque d'engorger votre programme.
{% end %}

Il est possible de se passer de l'implémentation du [trait](/rust/trait) `Clone` en passant par la [dérivation](/rust/derive).

```rust
#[derive(Clone)]
struct Toto;
```

Nous avons maintenant des armes pour travailler. 😁

{{ image(path="rust/move/clone1.png", width="100%") }}

Il faut voir le clone comme une machine qui prend en entré une variable clonable, en réalise une deuxième version exacte et rend la première version.

{{ image(path="rust/move/clone2.png", width="100%") }}

Notre scénario avec les déménageurs va donc se dérouler en deux temps:

Premièrement, on clone le carton `toto` en un deuxième carton `toto_clone`.

{{ image(path="rust/move/clone3.png", width="100%") }}

Puis on demande à l'équipe 1 de déménager le carton étiqueté `toto_clone` et à l'équipe 2 de déménager le carton étiqueté `toto`.

{{ image(path="rust/move/clone4.png", width="100%") }}

Chaque équipe trouve son carton, tout le monde est content. 😀

{% important() %}
Comme tout à l'heure, `toto` ayant été déplacé dans `function_1*`, il n'existe plus dans `main`.

Cela implique qu'il n'est plus clonable dans `main` et n'est pas déplaçable dans une troisième maison `function_1**`
{% end %}

## Copy

Il existe un deuxième mécanisme que nous avons d'ailleurs utilisé sans nous en rendre compte, il s'agit de la **copie**.

Pour pouvoir bénéficier de cette capacité une variable doit être copiable.

Cette attribut est fourni par le [trait](/rust/trait) `Copy`.

Le trait `Copy` est un [super trait](/rust/trait/#supertrait) de `Clone`. Cela implique que pour implémenter `Copy` une [structure](/rust/structure) doit obligatoirement
implémenter `Clone`.

Le trait `Copy` est ce qu'on appelle un [Marker](/rust/trait/#marker), un trait sans implémentation.

```rust
#[derive(Clone)]
struct Toto;

impl Copy for Toto {}
```

Il est également possible de passer la [dérivation](/rust/derive).

```rust
#[derive(Clone, Copy)]
struct Toto;
```

Celle-ci est fourni par le langage et ne peut pas être défini par le développeur.

Les [types primimitifs](/rust/types) sont tous copiables. A l'exeception des [chaîne de caractères](/rust/strings) qui sont en fait des [références](/rust/reference).

Une structure composée de types primitifs est donc copiable.

```rust
#[derive(Clone, Copy)]
struct Data {
    number: u64,
    float: f64,
    boolean: bool,
    array: [u8; 1024]
}
```

Quel super-pouvoirs cela nous octroie ?

Et bien, plus besoin de demander explicitement un clone via `.clone()` tout se fait magiquement !

```rust
fn main() {
    let toto = Toto;
    let toto_copy = toto; // la variable est ici copié
}
```

Ce qui implique que l'on peut écrire ceci

```rust
fn main() {
    let toto = Toto;
    function_1(toto); // toto est copié
    function_1(toto); // toto est copié
    function_1(toto); // toto est copié
    function_1(toto);
    // etc ...
    function_1(toto);
    // toto est toujours présent dans main !! 
}
```

Si on reprend la scenette des déménageurs, c'est comme si à chaque fois qu'on prenait un carton, celui-ci se dupliquait de lui-même.

Ce qui permet à chaque maison `function_1` d'avoir sa propre copie de `toto`.

Autre avantage, le `toto` originel ne quitte jamais la maison `main`!

{{ image(path="rust/move/copy1.png", width="100%") }}

Il est donc possible de déménager le carton `toto` autant de fois que l'on le désire.


{% important() %}
La copie étant implicite, il est possible de copier de grandes quantitée de données sans s'en rendre compte !
{% end %}

## Conclusion

Si une fonction désire l'ownership d'une variable il faut déplacer la variable du contexte appellant vers le contexte appelé.

Une fois qu'une variable est déplacée dans le contexte appelé, elle n'est plus disponible dans le contexte appellant.

Pour conserver la disponibilité d'une variable dans le contexte appellant, il faut dupliquer la variable.

Soit via un `Clone` expicite.

{{ image(path="rust/move/conclusion1.png", width="100%") }}

Soit via un `Copy` implicite.

{{ image(path="rust/move/conclusion2.png", width="100%") }}

Toute variable copiable est clonable mais l'inverse n'est pas vrai.